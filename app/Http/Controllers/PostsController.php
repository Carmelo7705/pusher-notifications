<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Auth;
use Session;

class PostsController extends Controller
{
    public function store(Request $request)
    {
      $request->validate([
            'title' => 'required|min:6',
            'content' => 'required|min:20'
        ]);
        Post::create([
            'title' => $request->title,
            'content' => $request->content,
            'user_id' => Auth::user()->id
        ]);
        Session::flash('status', 'A new Post was successfully created');
        return redirect()->back();
    }

    public function show($id)
    {
        $post = Post::find($id);
        return view('post.show')->withPost($post);
    }
}
