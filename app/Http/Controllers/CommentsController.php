<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use App\Comment;
use App\Post;
use App\Notifications\NotifyOwner;

class CommentsController extends Controller
{
  public function store(Request $request)
  {
      $request->validate([
          'comment' => 'required|min:4'
      ]);
      
      Comment::create([
          'user_id' => Auth::user()->id,
          'comment' => $request->comment,
          'post_id' => $request->post_id
      ]);

      $post = Post::find($request->post_id);
      $post->user->notify(new NotifyOwner($post));
      Session::flash('status', 'Comment was successfully created');
      return redirect()->back();
  }
}
